import os
from typing import List, Tuple


def generate_color_variants(icon_path: str, colors: List[str]) -> None:
    from PIL import Image

    # Chargement de l'icône d'origine
    original_icon = Image.open(icon_path)

    # Création du dossier de sortie
    os.makedirs("output", exist_ok=True)

    # Vider le dossier de sortie
    for file in os.listdir("output"):
        os.remove(os.path.join("output", file))

    # Boucle sur chaque couleur pour générer les variantes
    for color, index in zip(colors, range(len(colors))):
        # Utilisation de la bibliothèque PIL pour copier l'icône d'origine
        icon_copy = original_icon.copy()

        # Récupération des données de l'image sous forme de tableau de pixels
        pixels = icon_copy.load()

        # Utilisation de la bibliothèque PIL pour parcourir tous les pixels de l'image
        for x in range(icon_copy.width):
            for y in range(icon_copy.height):
                # Vérification si le pixel est noir
                if pixels[x, y] == (0, 0, 0, 255):
                    # Conversion de la couleur en format RGB
                    rgb_color = tuple(
                        int(color.lstrip("#")[i : i + 2], 16) for i in (0, 2, 4)
                    )

                    # Modification de la couleur du pixel
                    pixels[x, y] = rgb_color + (255,)

        # Enregistrer la variante colorée
        icon_copy.save(f"output/icon_{index}.png")


if __name__ == "__main__":
    couleurs = [
        "#00FF00",
        "#33FF00",
        "#66FF00",
        "#99FF00",
        "#CCFF00",
        "#FFFF00",
        "#FF9900",
        "#FF6600",
    ]

    generate_color_variants("icon.png", couleurs)
